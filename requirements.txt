Django==2.2.5
django-extensions==2.2.1
mysqlclient==1.4.4
pkg-resources==0.0.0
pytz==2019.2
six==1.12.0
sqlparse==0.3.0
