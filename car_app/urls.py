from django.urls import path
from django.views.generic import TemplateView
from .views import CarCheckInView,EmptySpaceList,AllocatedSpaceList,CarCheckOutView

urlpatterns = [

    path('dashboard/',TemplateView.as_view(template_name='dashboard.html')),
    path('empty-space/', EmptySpaceList.as_view(), name='empty-space'),
    path('allocated-space/', AllocatedSpaceList.as_view(), name='allocated-space'),
    path('car-checkin/',CarCheckInView.as_view(),name='car-checkin'),
    path('car-check-out/<int:pk>/',CarCheckOutView.as_view(),name='car-check-out'),

]