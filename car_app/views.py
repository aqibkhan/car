from django.shortcuts import render,redirect
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import CreateView,UpdateView,ListView
from .form import CarForm
from .models import ParkingSpace,Car
from django.shortcuts import get_object_or_404
from django.utils import timezone

# Create your views here

class EmptySpaceList(ListView):

    def get(self, request, *args, **kwargs):
        queryset = ParkingSpace.objects.filter(is_available=True).order_by('space_id')

        return render(request,'empty_space.html',{'empty_space':queryset})


class AllocatedSpaceList(ListView):

    def get(self, request, *args, **kwargs):

        car_info = Car.objects.filter(in_time__isnull=False, out_time__isnull=True).select_related('parking_space')

        return render(request,'allocated_space.html',{'allocated_space':car_info})

class CarCheckInView(CreateView):

    template_name = 'allocated_space.html'

    @csrf_exempt
    def post(self, request, *args, **kwargs):

        parking_space = get_object_or_404(ParkingSpace, id=self.request.POST['parking_pk'])

        print('parking space',parking_space)

        if parking_space.is_available:
            self.request.POST._mutable = True
            self.request.POST['parking_space'] = self.request.POST['parking_pk']
            form_class = CarForm(self.request.POST)

            if form_class.is_valid():
                form_class.save()
                parking_space.is_available = False
                parking_space.save()

        return redirect('empty-space')

class CarCheckOutView(UpdateView):

    def get(self, request, *args, **kwargs):
        id = self.kwargs['pk']
        if id:
            check_out = Car.objects.filter(id=id,in_time__isnull=False).first()
            if check_out.in_time:
                if not check_out.out_time:
                    parking_space = ParkingSpace.objects.filter(id=check_out.parking_space_id,is_available=False).first()
                    print(check_out.parking_space_id)
                    check_out.out_time = timezone.now()
                    parking_space.is_available = True
                    parking_space.save()
                    check_out.save()

        return redirect('allocated-space')
