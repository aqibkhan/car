from django import forms
from .models import ParkingSpace,Car

class ParkingSpaceForm(forms.ModelForm):
    class Meta:
        model = ParkingSpace
        exclude = {'created_at','updated_at'}

class CarForm(forms.ModelForm):

    class Meta:
        model = Car
        exclude = {'created_at', 'updated_at'}
